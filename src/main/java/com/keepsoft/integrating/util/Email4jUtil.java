package com.keepsoft.integrating.util;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//import com.keepsoft.monitoring.collection.dao.MonitoringDao;
//import com.keepsoft.monitoring.collection.model.Sensor;


public class Email4jUtil {
	
	public static String myEmailAccount = "m17612735616@163.com";
    public static String myEmailPassword = "lcw5201314";
    public static String myEmailSMTPHost = "smtp.163.com";
    public static String receiveMailAccount = "851555755@qq.com";
	
	public static void main(String[] args) {
		sendMailBySmtp("来自月球的黄梅佬!!!");
//		int corePoolSize = 1 ;
//		int initialDelay = 1;
//		int period = 600000000;
//		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});
//		MonitoringDao monitoringDao = (MonitoringDao) context.getBean("monitoringDao");
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(corePoolSize);
//
//		scheduledExecutorService.scheduleAtFixedRate(new TimerTask() {
//			@Override
//			public void run() {
//				try {
//					List<Sensor> list = monitoringDao.getSensors();
//					Map<Long,String> map = new HashMap<Long,String>();
//					for(int i=0;i<list.size();i++){
//						String lastTime = monitoringDao.getLastMonitoringData(list.get(i).getCode());
//						String nowTime = df.format(new Date());
//						String testTime = "2017-10-27";
//						if(lastTime == null){
//							map.put(list.get(i).getCode(), lastTime);
//
//						}else{
//							if(!lastTime.substring(0, 10).equals(nowTime.substring(0, 10))){
//						        map.put(list.get(i).getCode(), lastTime);
//							}
//
//						}
//
//					}
//					String msgContext="";
//			        for (Map.Entry<Long, String> entry : map.entrySet()) {
//			        	msgContext += entry.getKey()+" 监测设备出现异常,最近采集数据时间为："+entry.getValue()+"\n";
//			        }
//				    System.out.println(msgContext);
//				    if(msgContext.length()>0){
//				    	sendMailBySmtp("测试");
//				    }

//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
//			}
//		}, initialDelay, period, TimeUnit.SECONDS);
		//sendMailBySmtp();
		
		/*Thread t = new Thread(new Runnable() {			
			@Override
			public void run() {
				for(int i=0;i<100;i++){
					System.out.println("++++++++++++++++++++++");
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				
			}
		});
		t.start();*/
	}
	
	
	public static void sendMailBySmtp(String msgContext){
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp"); 
		props.setProperty("mail.smtp.host", myEmailSMTPHost); 
		props.setProperty("mail.smtp.auth", "true"); 
	    Session session = Session.getInstance(props);
	    //session.setDebug(true);  

	    try {
	        MimeMessage message = createMimeMessage(session, myEmailAccount, receiveMailAccount);
	        message.setContent(msgContext, "text/html;charset=UTF-8");
	        Transport transport = session.getTransport();
	        transport.connect(myEmailAccount, myEmailPassword);
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	    } catch (Exception e) {
	        System.out.println("send failed, exception: " + e);
	    }
	}
	
	
	public static MimeMessage createMimeMessage(Session session, String sendMail, String receiveMail) throws Exception {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(sendMail, "来自月球的黄梅佬", "UTF-8"));
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, "", "UTF-8"));
        message.setSubject("不是垃圾邮件", "UTF-8");
        message.setSentDate(new Date());
        message.saveChanges();
        return message;
    }
	
	
}
