package com.keepsoft.integrating.util;

import org.springframework.util.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author liuChengWen.
 * @date 2018\6\7 0007 10:04
 * @desc
 */
public class analysisFile {
    public static void main(String[] args) {
        List text=analysis(new File("C:\\Users\\Administrator\\Desktop\\monitor_Depth.plt"));
        for (int i = 0; i <text.size() ; i++) {
            if(i==0){
                System.out.println(text.get(i));
            }
        }
    }

    public static List analysis(File file){
        ArrayList list=new ArrayList();
        try {
            String text;
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(new FileInputStream(file),"utf-8"));
            while ((text=bufferedReader.readLine()) !=null){
                text=text.replaceAll(" ","");
                String[] strs=text.split(",");
                text=Arrays.toString(strs);
                list.add(text);
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

}
