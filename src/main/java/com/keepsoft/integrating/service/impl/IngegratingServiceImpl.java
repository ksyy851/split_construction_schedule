package com.keepsoft.integrating.service.impl;

import java.util.List;

import com.keepsoft.integrating.dao.IntegratingDao;
import com.keepsoft.integrating.service.IntegratingService;

public class IngegratingServiceImpl implements IntegratingService {
	
	private IntegratingDao integratingDao;

	public void setIntegratingDao(IntegratingDao integratingDao) {
		this.integratingDao = integratingDao;
	}

	public List<String> getEquNames(Integer type){
		return integratingDao.getEquNames(type);
	}
	
	public String get2DaysAgo(Integer type,String name){
		return integratingDao.get2DaysAgo(type, name);
	}
	
	public String getMinTime(Integer type,String name){
		return integratingDao.getMinTime(type, name);
	}
	
	public Integer saveBackupData(Integer type,String name,String time){
		return integratingDao.saveBackupData(type, name, time);
	}
	
	public Integer saveAvgData(Integer type,String name,String time){
		return integratingDao.saveAvgData(type, name, time);
	}
	
	public Integer remove2DaysAgoData(Integer type,String name,String time){
		return integratingDao.remove2DaysAgoData(type, name, time);
	}
	
	public void integratingData(Integer type,String name,String time){
		saveBackupData(type, name, time);
		saveAvgData(type, name, time);
		remove2DaysAgoData(type, name, time);
	}

	@Override
	public void insertData(Integer type) {
		integratingDao.insertData(type);
	}
}
