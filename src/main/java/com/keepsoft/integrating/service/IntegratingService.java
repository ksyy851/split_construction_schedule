package com.keepsoft.integrating.service;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.keepsoft.integrating.dao.IntegratingDao;



public interface IntegratingService {
	
	List<String> getEquNames(Integer type);
	
	String get2DaysAgo(Integer type,String name);
	
	String getMinTime(Integer type,String name);
	
	Integer saveBackupData(Integer type,String name,String time);
	
	Integer saveAvgData(Integer type,String name,String time);
	
	Integer remove2DaysAgoData(Integer type,String name,String time);
	
	void integratingData(Integer type,String name,String time);
	
	void insertData(Integer type);
	

}
