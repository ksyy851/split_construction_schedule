package com.keepsoft.integrating.main;

import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.keepsoft.integrating.service.IntegratingService;

public class Main {
	public static final Logger logger = LoggerFactory.getLogger(Main.class); 

	public static Integer YEAR=2018;
	public static Integer MONTH=1;
	public static Integer DAY=24;

	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});
		IntegratingService integratingService = (IntegratingService) context.getBean("integratingService");
		
		int corePoolSize = 4;//线程池个数
		int initialDelay = 5;//任务第一次启动的时间（单位：s/秒）
		int period = 120;//任务间隔时间(单位：s/秒)
	
		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(corePoolSize);
		scheduledExecutorService.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				integratingData(1,integratingService);
				long endTime = System.currentTimeMillis();
				logger.info("YL run time ：" + (endTime - startTime) + "ms"); //795375ms
			}
		}, initialDelay, period, TimeUnit.SECONDS);

		scheduledExecutorService.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				integratingData(0,integratingService);
				long endTime = System.currentTimeMillis();
				logger.info("SL run time：" + (endTime - startTime) + "ms"); //795375ms
			}
		}, 2, period, TimeUnit.SECONDS);

		scheduledExecutorService.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				insertData(0,integratingService);
			}
		},initialDelay,period,TimeUnit.SECONDS);

		scheduledExecutorService.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				insertData(1,integratingService);
			}
		},2,period,TimeUnit.SECONDS);
	}
	
	public  static void integratingData(Integer type,IntegratingService integratingService){
		List<String> equNames = integratingService.getEquNames(type);
		int i = 0;
		String dayTime;
		String minTime;
		for(String equName:equNames){
			dayTime = integratingService.get2DaysAgo(type, equName);
			minTime = integratingService.getMinTime(type, equName);
			i++;
			if(dayTime.compareTo(minTime)>0){
				integratingService.integratingData(type, equName, dayTime);
				logger.info(i+" "+equName+" "+dayTime+" "+minTime+" saved ...");
			}else{
				logger.info(i+" "+equName+" "+dayTime+" "+minTime+" unresolve ...");
			}
		}
	}

	/***
	 * 模拟插入数据
	 * @param type
	 */
	public static void insertData(Integer type,IntegratingService integratingService){
		integratingService.insertData(type);
	}

}
