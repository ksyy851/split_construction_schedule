package com.keepsoft.integrating.model;

public class FlowData {
	private Integer id;
	private String name;
	private Double temperature;
	private Double depth;//水深
	private Double speed;//流速
	private Double instantFlow;//瞬时流量，立方米每小时,需转换单位为m³每秒
	private Double accumFlowInt;
	private Double accumFlowFloat;
	private String time;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public Double getDepth() {
		return depth;
	}
	public void setDepth(Double depth) {
		this.depth = depth;
	}
	public Double getSpeed() {
		return speed;
	}
	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	public Double getInstantFlow() {
		return instantFlow;
	}
	public void setInstantFlow(Double instantFlow) {
		this.instantFlow = instantFlow;
	}
	public Double getAccumFlowInt() {
		return accumFlowInt;
	}
	public void setAccumFlowInt(Double accumFlowInt) {
		this.accumFlowInt = accumFlowInt;
	}
	public Double getAccumFlowFloat() {
		return accumFlowFloat;
	}
	public void setAccumFlowFloat(Double accumFlowFloat) {
		this.accumFlowFloat = accumFlowFloat;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return "FlowData [id=" + id + ", name=" + name + ", temperature=" + temperature + ", depth=" + depth
				+ ", speed=" + speed + ", instantFlow=" + instantFlow + ", accumFlowInt=" + accumFlowInt
				+ ", accumFlowFloat=" + accumFlowFloat + ", time=" + time + "]";
	}
	
	
	

}
