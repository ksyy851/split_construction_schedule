package com.keepsoft.integrating.dao;

import java.util.List;

public interface IntegratingDao {
	
	List<String> getEquNames(Integer type);
	
	String get2DaysAgo(Integer type,String name);
	
	String getMinTime(Integer type,String name);
	
	Integer saveBackupData(Integer type,String name,String time);
	
	Integer saveAvgData(Integer type,String name,String time);
	
	Integer remove2DaysAgoData(Integer type,String name,String time);
	
	void insertData(Integer type);

}
